﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }
    
        async void OnButtonClicked(object sender, EventArgs e) // this menthod will be called on clicking on clicked button
        {
            await Navigation.PushAsync(new Page5());
        }

    }

}