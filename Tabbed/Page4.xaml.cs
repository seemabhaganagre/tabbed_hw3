﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page4 : ContentPage
    {
        public Page4()
        {
            InitializeComponent();
        }
        async void OnRotateAnimationButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page6()); //navigated to page 6
        }
        async void OnFadeAnimationButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page7()); //navigated to page 7
        }
    }
}

