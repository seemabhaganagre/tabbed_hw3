﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page5 : ContentPage
    {
        int clickTotal;
        public Page5()
        {
            InitializeComponent();
        }

       
        async void OnImageButtonClicked(object Sender, EventArgs e) // method is called when the image is clicked
        {
            {
                clickTotal += 1;
                label.Text = $"{clickTotal} ImageButton click{(clickTotal == 1 ? "" : "s")}"; // counts the number of time the image was clicked
            }

            bool usersResponse = await DisplayAlert("Info", "Guess the name of this national park", "I don't Know it", "I know it"); // alert

            if (usersResponse == true)
            {
                label.Text = $"That's Yosemite National Park";
            }
        }
        async void OnImageButtonClicked1(object Sender, EventArgs e) // method is called when the image is clicked
        {
            {
                clickTotal += 1;
                label.Text = $"{clickTotal} ImageButton click{(clickTotal == 1 ? "" : "s")}"; // counts the number of time the image was clicked
            } 

            bool usersResponse = await DisplayAlert("Info", "Guess the name of this national park", "I don't Know it", "I know it"); // alert

            if (usersResponse == true)
            {
                label.Text = $"That's Zion National Park"; 
            }
        }

        async void OnImageButtonClicked2(object Sender, EventArgs e) // method is called when the image is clicked
        {
            {
                clickTotal += 1;
                label.Text = $"{clickTotal} ImageButton click{(clickTotal == 1 ? "" : "s")}"; // counts the number of time the image was clicked
            }

            bool usersResponse = await DisplayAlert("Info", "Guess the name of this national park", "I don't Know it", "I know it"); // alert

            if (usersResponse == true)
            {
                label.Text = $"That's Saguaro National Park";
            }
        }

        async void OnImageButtonClicked3(object Sender, EventArgs e) // method is called when the image is clicked
        {
            {
                clickTotal += 1; 
                label.Text = $"{clickTotal} ImageButton click{(clickTotal == 1 ? "" : "s")}"; // counts the number of time the image was clicked
            }

            bool usersResponse = await DisplayAlert("Info", "Guess the name of this national park", "I don't Know it", "I know it"); // alert

            if (usersResponse == true)
            {
                label.Text = $"That's Alaska National Park";
            }
        }

        async void OnButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}