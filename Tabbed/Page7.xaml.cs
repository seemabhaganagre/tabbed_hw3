﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page7 : ContentPage
    {
        public Page7()
        {
            InitializeComponent();
        }

        void SetIsEnabledButtonState(bool startButtonState, bool cancelButtonState)
        {
            startButton.IsEnabled = startButtonState;
            cancelButton.IsEnabled = cancelButtonState;
        }

        async void OnStartAnimationButtonClicked1(object sender, System.EventArgs e) //starting animation
        {
            SetIsEnabledButtonState(false, true);

            image.Opacity = 0;
            await image.FadeTo(1, 4000);

            SetIsEnabledButtonState(true, false);
        }

        void OnCancelAnimationButtonClicked1(object sender, System.EventArgs e) // to stop animation.
        {
            ViewExtensions.CancelAnimations(image);
            SetIsEnabledButtonState(true, false);
        }
    }
}

