﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page8 : ContentPage
    {
        int clickTotal;
        public Page8()
        {
            InitializeComponent();
        }


        async void OnImageButtonClicked(object Sender, EventArgs e) // acalled on image clicked
        {
            {
                clickTotal += 1;
                label.Text = $"{clickTotal} ImageButton click{(clickTotal == 1 ? "" : "s")}"; //counts the number of times image was clicked
            }

            bool usersResponse = await DisplayAlert("Info", "Guess the name of this Mountain Range", "I don't Know it", "I know it"); //alert

            if (usersResponse == true)
            {
                label.Text = $"That's Mont Blanc";
            }
        }
        async void OnImageButtonClicked1(object Sender, EventArgs e) // called when image is clicked
        {
            {
                clickTotal += 1;
                label.Text = $"{clickTotal} ImageButton click{(clickTotal == 1 ? "" : "s")}"; //counts the number of times the image was clicked
            }

            bool usersResponse = await DisplayAlert("Info", "Guess the name of this Mountain Range", "I don't Know it", "I know it"); //alert

            if (usersResponse == true)
            {
                label.Text = $"That's Mount Everest";
            }
        }

        async void OnImageButtonClicked2(object Sender, EventArgs e) // called when the image is clicked
        {
            {
                clickTotal += 1;
                label.Text = $"{clickTotal} ImageButton click{(clickTotal == 1 ? "" : "s")}"; //counts the number of times the image was clicked
            }

            bool usersResponse = await DisplayAlert("Info", "Guess the name of this Mountain Range", "I don't Know it", "I know it"); //alert

            if (usersResponse == true)
            {
                label.Text = $"That's Kanchangunga";
            }
        }

        async void OnImageButtonClicked3(object Sender, EventArgs e)
        {
            {
                clickTotal += 1;
                label.Text = $"{clickTotal} ImageButton click{(clickTotal == 1 ? "" : "s")}";
            }

            bool usersResponse = await DisplayAlert("Info", "Guess the name of this Mountain Range", "I don't Know it", "I know it");

            if (usersResponse == true)
            {
                label.Text = $"That's Denali";
            }
        }

        async void OnButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
        }
    }
}