﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page6 : ContentPage
    {
        public Page6()
        {
            Title = "Rotate Animation";

            image = new Image { Source = ImageSource.FromFile("f1.jpeg"), VerticalOptions = LayoutOptions.CenterAndExpand }; //image being called
            startButton = new Button { Text = "Start Animation", VerticalOptions = LayoutOptions.End };
            cancelButton = new Button { Text = "Cancel Animation", IsEnabled = false };

            startButton.Clicked += OnStartAnimationButtonClicked;
            cancelButton.Clicked += OnCancelAnimationButtonClicked;

            Content = new StackLayout
            {
                Margin = new Thickness(0, 20, 0, 0),
                Children = {
image,
startButton,
cancelButton
}
            };
        }

        void SetIsEnabledButtonState(bool startButtonState, bool cancelButtonState)
        {
            startButton.IsEnabled = startButtonState;
            cancelButton.IsEnabled = cancelButtonState;
        }

        async void OnStartAnimationButtonClicked(object sender, EventArgs e) //to start the annimation
        {
            SetIsEnabledButtonState(false, true);

            await image.RotateTo(360, 2000);
            image.Rotation = 0;

            SetIsEnabledButtonState(true, false);
        }

        void OnCancelAnimationButtonClicked(object sender, EventArgs e) //to stop the animation
        {
            ViewExtensions.CancelAnimations(image);
            SetIsEnabledButtonState(true, false);
        }
    }
}
